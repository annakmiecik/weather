import * as api from '../api/weather'
import * as types from '../constants/actionTypes'

export const fetchWeather = (city = 'Warszawa') => dispatch => {
  dispatch(requestfetchWeather())

  return api
    .fetchWeather(city, 'pl')
    .then(response => {
      if (response.status === 200) {
        return response.data
      } else {
        throw new Error(response.data)
      }
    })
    .then(data => {
      dispatch(successfetchWeather(data))
    })
    .catch(error => {
      dispatch(errorfetchWeather(error))
    })
}

const successfetchWeather = json => ({
  type: types.FETCH_WEATHER_SUCCESS,
  payload: json
})
const requestfetchWeather = () => ({
  type: types.FETCH_WEATHER_REQUEST
})
const errorfetchWeather = error => ({
  type: types.FETCH_WEATHER_ERROR,
  payload: error
})
