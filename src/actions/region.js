import * as api from '../api/region'
import * as types from '../constants/actionTypes'
import { filter as _filter } from 'lodash/collection'

export const fetchCities = (region = 'Wojewodztwo Zachodniopomorskie') => dispatch => {
  dispatch(requestfetchCities())

  return api
    .fetchCities(region)
    .then(response => {
      if (response.status === 200) {
        return response.data
      } else {
        throw new Error(response.data)
      }
    })
    .then(data => {
      const filteredData = successfetchCities(_filter(data, o => !o.city.includes('Powiat')))
      dispatch(filteredData)
      return filteredData.payload
    })
    .catch(error => {
      dispatch(errorfetchCities(error))
    })
}

const successfetchCities = json => ({
  type: types.FETCH_CITIES_SUCCESS,
  payload: json
})
const requestfetchCities = () => ({
  type: types.FETCH_CITIES_REQUEST
})
const errorfetchCities = error => ({
  type: types.FETCH_CITIES_ERROR,
  payload: error
})

export const setCity = city => dispatch => {
  dispatch({
    type: types.SET_CITY_SUCCESS,
    payload: city
  })
}

export const fetchRegions = (country = 'pl') => dispatch => {
  dispatch(requestfetchRegions())

  return api
    .fetchRegions(country)
    .then(response => {
      if (response.status === 200) {
        return response.data
      } else {
        throw new Error(response.data)
      }
    })
    .then(data => {
      dispatch(successfetchRegions(data))
      return data
    })
    .catch(error => {
      dispatch(errorfetchRegions(error))
    })
}

const successfetchRegions = json => ({
  type: types.FETCH_REGIONS_SUCCESS,
  payload: json
})
const requestfetchRegions = () => ({
  type: types.FETCH_REGIONS_REQUEST
})
const errorfetchRegions = error => ({
  type: types.FETCH_REGIONS_ERROR,
  payload: error
})

export const setRegion = region => dispatch => {
  dispatch({
    type: types.SET_REGION_SUCCESS,
    payload: region
  })
}
