import axios from 'axios'

const BASE_URL = process.env.REACT_APP_WEATHER_API_BASE_URL
const ICON_URL = process.env.REACT_APP_WEATHER_API_ICON_URL
const API_KEY = process.env.REACT_APP_WEATHER_API_KEY

export const fetchWeather = (city, country = 'pl') => {
  const config = {
    baseURL: BASE_URL,
    params: {
      key: API_KEY,
      lang: 'pl',
      units: 'm',
      city: city,
      country: country
    }
  }

  return axios.get(`${BASE_URL}`, config)
}

export const fetchIconUrl = icon => `${ICON_URL}${icon}.png`
export default fetchWeather
