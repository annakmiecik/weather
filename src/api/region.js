import axios from 'axios'

const CITIES_URL = process.env.REACT_APP_CITIES_API_BASE_URL
const REGION_URL = process.env.REACT_APP_REGIONS_API_BASE_URL
const API_KEY = process.env.REACT_APP_REGION_API_KEY

export const fetchRegions = (country = 'pl') => {
  const url = `${REGION_URL}${country}/all/`
  const config = {
    baseURL: url,
    params: {
      key: API_KEY
    }
  }

  return axios.get(url, config)
}

export const fetchCities = (region = 'Wojewodztwo Zachodniopomorskie') => {
  const config = {
    baseURL: CITIES_URL,
    params: {
      key: API_KEY,
      region: region
    }
  }

  return axios.get(`${CITIES_URL}`, config)
}
