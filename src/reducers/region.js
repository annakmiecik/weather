import * as types from '../constants/actionTypes'
import _ from 'lodash/object'
import { combineReducers } from 'redux'

const initialState = {
  isError: false,
  isFetching: false,
  items: [],
  selected: ''
}

function regionsReducer(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_REGIONS_SUCCESS: {
      const region = _.mapKeys(action.payload, 'region')
      return {
        ...state,
        items: { ...region },
        ...{
          selected:
            action.payload && action.payload[0] && action.payload[0].region
              ? action.payload[0].region
              : null
        },
        isError: false,
        isFetching: false
      }
    }
    case types.FETCH_REGIONS_ERROR:
      return { ...state, isError: true, isFetching: false }
    case types.FETCH_REGIONS_REQUEST:
      return { ...state, isError: false, isFetching: true }
    case types.SET_REGION_SUCCESS:
      return { ...state, selected: action.payload }
    default:
      return state
  }
}

function cityReducer(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_CITIES_SUCCESS: {
      const city = _.mapKeys(action.payload, 'city')
      return {
        ...state,
        items: { ...city },
        ...{ selected: action.payload && action.payload[0].city ? action.payload[0].city : null },
        isError: false,
        isFetching: false
      }
    }
    case types.FETCH_CITIES_ERROR:
      return { ...state, isError: true, isFetching: false }
    case types.FETCH_CITIES_REQUEST:
      return { ...state, isError: false, isFetching: true }
    case types.SET_CITY_SUCCESS:
      return { ...state, selected: action.payload }
    default:
      return state
  }
}

const rootReducer = combineReducers({
  cities: cityReducer,
  regions: regionsReducer
})

export default rootReducer
