import { combineReducers } from 'redux'
import weather from './weather'
import region from './region'
import createHistory from 'history/createBrowserHistory'
// 'routerMiddleware': the new way of storing route changes with redux middleware since rrV4.
import { connectRouter } from 'connected-react-router'

const history = createHistory()
const connectRouterHistory = connectRouter(history)

const rootReducer = combineReducers({
  region,
  router: connectRouterHistory,
  weather
})

export default rootReducer
