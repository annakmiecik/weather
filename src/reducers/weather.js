import * as types from '../constants/actionTypes'

const initialState = {
  isError: false,
  isFetching: false,
  item: {}
}

export default function weatherReducer(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_WEATHER_SUCCESS: {
      return { ...state, item: action.payload.data[0], isError: false, isFetching: false }
    }
    case types.FETCH_WEATHER_ERROR:
      return { ...state, isError: true, isFetching: false }
    case types.FETCH_WEATHER_REQUEST:
      return { ...state, isError: false, isFetching: true }

    default:
      return state
  }
}
