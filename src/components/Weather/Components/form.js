import React, { Component } from 'react'
import { connect } from 'react-redux'
import FormSelectGroup from '../../FormSelectGroup'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'

import { fetchCities, fetchRegions, setCity, setRegion } from '../../../actions/region'
import { fetchWeather } from '../../../actions/weather'

class RegionsFormComponent extends Component {
  componentDidMount() {
    const { fetchRegions } = this.props
    fetchRegions()
  }

  componentDidUpdate(prevProps) {
    const { fetchCities, fetchWeather, cities, regions } = this.props

    if (cities.selected !== prevProps.cities.selected) {
      fetchWeather(cities.selected)
    }

    if (regions.selected !== prevProps.regions.selected) {
      fetchCities(regions.selected)
    }
  }

  handleRegionChange = event => {
    const { fetchCities, setRegion } = this.props
    const { name, value } = event.target
    this.setState({ [name]: value, validated: false })
    setRegion(value)

    fetchCities(value)
  }

  handleCityChange = event => {
    const { setCity } = this.props
    const { name, value } = event.target
    this.setState({ [name]: value, validated: false })
    setCity(value)
  }

  renderForm = () => {
    const { cities, regions = {} } = this.props
    return (
      <Form noValidate>
        <Form.Row>
          <Col>
            <FormSelectGroup
              name="Region"
              placeholder="Region"
              prepend="Region"
              onChange={this.handleRegionChange}
              required={true}
              className="mb-3"
              value={regions.selected}
              error="Region is required"
            >
              {regions && regions.items
                ? Object.keys(regions.items).map(region => (
                    <option key={region} value={regions.items[region].region}>
                      {regions.items[region].region}
                    </option>
                  ))
                : null}
            </FormSelectGroup>
          </Col>
        </Form.Row>
        <Form.Row>
          <Col>
            <FormSelectGroup
              name="City"
              placeholder="City"
              prepend="City"
              onChange={this.handleCityChange}
              required={true}
              className="mb-3"
              value={cities.selected}
              error="Region is required"
            >
              {cities && cities.items
                ? Object.keys(cities.items).map(city => (
                    <option key={city} value={cities.items[city].city}>
                      {cities.items[city].city}
                    </option>
                  ))
                : null}
            </FormSelectGroup>
          </Col>
        </Form.Row>
      </Form>
    )
  }

  render() {
    const { regionApiHasError = false, city_name } = this.props
    return (
      <React.Fragment>
        {regionApiHasError ? (
          <div>
            <h2>{city_name}</h2>
            <p className="weatherError">Error occured in region api!</p>
          </div>
        ) : (
          this.renderForm()
        )}
      </React.Fragment>
    )
  }
}

const mapDispatchToProps = {
  fetchCities,
  fetchRegions,
  fetchWeather,
  setCity,
  setRegion
}

const mapStateToProps = ({ region: { cities, regions } }) => ({
  cities: cities,
  regions: regions,
  regionApiHasError: regions.isError
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegionsFormComponent)
