import React from 'react'
import * as PropTypes from 'prop-types'
import { fetchIconUrl } from '../../../api/weather'
import tempIcon from '../../../assets/images/Sustainable icon.png'

import './style.scss'

class Weather extends React.Component {
  state = {
    icon: null
  }

  componentDidMount() {
    this.refreshIcon()
  }

  componentDidUpdate(prevProps) {
    const { icon } = this.props
    if (prevProps.icon !== icon) {
      this.refreshIcon()
    }
  }

  refreshIcon = () => {
    const { icon } = this.props
    if (icon !== '') {
      const iconUrl = fetchIconUrl(icon)
      this.setState({ icon: iconUrl })
    }
  }

  render() {
    const { temp, cityName, wind, description, error } = this.props
    const { icon } = this.state

    return (
      <div className="weather-info px-auto mx-5">
        {icon ? (
          <img src={icon} alt="Temp icon" className="my-4 mx-auto" />
        ) : (
          <React.Fragment>
            <img src={tempIcon} alt="Error icon" width="100" className="my-4 text-align-center" />
            <p className="weatherError">Error occured in weather api!</p>
          </React.Fragment>
        )}

        <section className="weatherTable">
          {temp && (
            <React.Fragment>
              <h3 className="item-a font-weight-bold">temp:</h3>
              <div className="item-b">{temp}</div>
            </React.Fragment>
          )}

          {cityName && (
            <React.Fragment>
              <h3 className="item-a font-weight-bold">city:</h3>
              <div className="item-b">{cityName}</div>
            </React.Fragment>
          )}

          {icon && (
            <React.Fragment>
              <h3 className="item-a font-weight-bold">icon url:</h3>
              <div className="item-b">{icon}</div>
            </React.Fragment>
          )}

          {wind && (
            <React.Fragment>
              <h3 className="item-a font-weight-bold">wind:</h3>
              <div className="item-b">{wind}</div>
            </React.Fragment>
          )}

          {description && (
            <React.Fragment>
              <h3 className="item-a font-weight-bold">conditions:</h3>
              <div className="item-b">{description}</div>
            </React.Fragment>
          )}

          {error && <p className="item-g font-weight-bold">{error}</p>}
        </section>
      </div>
    )
  }
}

Weather.propTypes = {
  temp: PropTypes.number,
  cityName: PropTypes.string.isRequired,
  wind: PropTypes.string,
  description: PropTypes.string,
  icon: PropTypes.string,
  error: PropTypes.string
}

Weather.defaultProps = {
  icon: ''
}

export default Weather
