import React, { Component } from 'react'
import { connect } from 'react-redux'
import Row from 'react-bootstrap/Row'
import logo from '../../logo.svg'
import './style.scss'

import { fetchWeather } from '../../actions/weather'
import RegionForm from './Components/form'
import Weather from './Components/weather'

import cityIcon from '../../assets/images/City icon.png'

class WeatherComponent extends Component {
  componentDidUpdate(prevProps) {
    const { fetchWeather, regionApiHasError } = this.props

    if (regionApiHasError !== prevProps.regionApiHasError && regionApiHasError) {
      //region api has error or request count exceeded
      fetchWeather('Wrocław')
    }
  }

  render() {
    const {
      weatherIn: {
        isError,
        isFetching,
        item: {
          city_name = '',
          wind_cdir_full,
          temp,
          weather: { description = '', icon = '' } = {}
        } = {}
      } = {}
    } = this.props
    return (
      <div className="align-items-center d-flex flex-column align-items-center justify-content-center App App-header">
        <img src={logo} alt="logo" width="100" height="100" />
        <h1 className="title-container__title">Weather Scanner </h1>
        <p className="title-container__subtitle">Helps you find weather conditions in cities...</p>
        <img alt="City icon" className="my-4" src={cityIcon} width="200" height="200" />

        <Row noGutters>
          <RegionForm city_name={city_name} />
        </Row>
        <Row noGutters>
          {isFetching || isError ? null : (
            <Weather
              temp={temp}
              cityName={city_name}
              wind={wind_cdir_full}
              description={description}
              icon={icon}
              error={null}
            />
          )}
        </Row>
      </div>
    )
  }
}

const mapDispatchToProps = {
  fetchWeather
}

const mapStateToProps = ({ region, weather }) => ({
  weatherIn: weather,
  regionApiHasError: region.regions.isError
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WeatherComponent)
