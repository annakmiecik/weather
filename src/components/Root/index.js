import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import * as PropTypes from 'prop-types'

import './style.scss'
import '../../styles/bootstrap-custom.scss'
import Weather from '../Weather'

function App() {
  return (
    <div className="App">
      <Weather />
    </div>
  )
}

export default class Root extends Component {
  render() {
    const { store, history } = this.props
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </Provider>
    )
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}
