import React from 'react'
import * as PropTypes from 'prop-types'
import InputGroup from 'react-bootstrap/InputGroup'
import Form from 'react-bootstrap/Form'

const FormSelectGroup = ({
  name,
  prepend,
  label,
  onChange,
  required,
  error,
  className,
  value,
  placeholder,
  children
}) => {
  return (
    <InputGroup className={className} size="lg">
      {label && <Form.Label>{label}</Form.Label>}
      {prepend && (
        <InputGroup.Prepend>
          <InputGroup.Text>{prepend}</InputGroup.Text>
        </InputGroup.Prepend>
      )}
      <Form.Control
        id={name}
        name={name}
        value={value}
        as="select"
        placeholder={placeholder}
        aria-label={placeholder}
        aria-describedby={name}
        onChange={onChange}
        required={required}
      >
        {children}
      </Form.Control>
      {error && <Form.Control.Feedback type="invalid">{error}</Form.Control.Feedback>}
    </InputGroup>
  )
}

FormSelectGroup.propTypes = {
  className: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  prepend: PropTypes.any,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool.isRequired,
  error: PropTypes.string,
  value: PropTypes.string.isRequired,
  children: PropTypes.any
}

export default FormSelectGroup
